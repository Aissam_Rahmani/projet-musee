package com.example.projetmusee;

import android.net.Uri;

import java.net.MalformedURLException;
import java.net.URL;

public class WebServiceUrlMusee {

    private static final String HOST = "demo-lia.univ-avignon.fr";
    private static final String PATH_1 = "cerimuseum";

    private static Uri.Builder commonBuilder() {
        Uri.Builder builder = new Uri.Builder();

        builder.scheme("https")
                .authority(HOST)
                .appendPath(PATH_1);
        return builder;
    }


    // https://demo-lia.univ-avignon.fr/cerimuseum/ids
    private static final String IDS = "ids";

    //  get all id of all artefacts
    public static URL buildSearchAllId() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(IDS);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get all categories
    // https://demo-lia.univ-avignon.fr/cerimuseum/categories
    private static final String CATEGORIES = "categories";
    public static URL buildSearchAllCategories() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(CATEGORIES);
        URL url = new URL(builder.build().toString());
        return url;
    }


    // https://demo-lia.univ-avignon.fr/cerimuseum/catalog
    private static final String CATALOG = "catalog";
    public static URL buildSearchAllCatalog() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(CATALOG);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // get les information de chaque objet
    private static final String ITEMS = "items";
    public static URL buildSearchItemInformation(String idItem) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(ITEMS)
                .appendPath(idItem);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // get PNG thumbnail of given item
    private static final String THUMBNAIL_PNG = "thumbnail";
    public static URL buildSearchPngThumbnail(String idItem) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(ITEMS)
                .appendPath(idItem)
                .appendPath(THUMBNAIL_PNG);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get image  JPEG of given item
    private static final String IMAGE_JPEG = "images";
    public static URL buildSearchJpegImage(String idItem, String imageId) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(ITEMS)
                .appendPath(idItem)
                .appendPath(IMAGE_JPEG)
                .appendPath(imageId);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Dictionnaire des dates/heurs des 10 prochaines séances de déonstration de certain Item.
    private static final String DEMOS = "demos";
    public static URL builSearchDemonstrations() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(DEMOS);
        URL url = new URL(builder.build().toString());
        return url;
    }
}
