package com.example.projetmusee;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class ItemActivity extends AppCompatActivity {

    private  Item item;
    String idItem;


    @Override
        protected void onCreate(Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_detail);

            Intent intent = getIntent();

            item = intent.getParcelableExtra("item");
            idItem = item.getIdName();


        ImageView imageItem = findViewById(R.id.imageItem);
        try{
            if (item.getThumbail() != null){ new MyAdapter.LoadImageTask(imageItem, this.item).execute();}

            String urlImg = WebServiceUrlMusee.buildSearchPngThumbnail(idItem).toString();
            if (item.getThumbail() != null){ new MyAdapter.LoadImageTask(imageItem, this.item).execute();}
        }
        catch (Exception e){
            e.printStackTrace();
        }
		if(item.getYear() != null){
			TextView annee = findViewById(R.id.yearItem);
			annee.setText(item.getYear());
		}
    // Inserer le nom de l'artefact
        TextView name = findViewById(R.id.name);
        name.setText(item.getName());

        //Les das décennie
        TextView timeFrame = findViewById(R.id.timeFrameItem);
        timeFrame.setText(TextUtils.join(" ; ", item.getTimeFrame()));

        MyAsyntask datedece = new MyAsyntask();
        datedece.execute();

        TextView itemdescriptionn = findViewById(R.id.descriptionItem);
        itemdescriptionn.setText(item.getDescription());


        TextView categories = findViewById(R.id.categoriesItem);
        categories.setText(TextUtils.join(" ; ", item.getCategories()));

        if(item.getBrand() != null){
			TextView brandItem = findViewById(R.id.brandItem);
			brandItem.setText(item.getBrand());
		}

        if(item.getTechDetails() != null){
			TextView techDetail = findViewById(R.id.techDetailsItem);
			techDetail.setText(TextUtils.join(" ; ", item.getTechDetails()));
		}

        if(item.getWorking() != null){
			TextView boolWorking = findViewById(R.id.workingItem);
			if(item.getWorking() != null){
                if(item.getWorking().toLowerCase().contains("true")){
                    boolWorking.setText("En état de fonctionnement");
                }
                else{
                    boolWorking.setText("Non fonctionnement");
                }
            }

		}

                // Voir les autres image s'il existent

			Button button = findViewById(R.id.button);


			button.setOnClickListener(new View.OnClickListener(){

                @Override
				public void onClick(View v){

                    if(!((item.getPicture().get(0)).equals("null"))){
					Intent myIntent = new Intent(getApplicationContext(), ActivityAvecImage.class);
					myIntent.putExtra("possedImages", item);
					v.getContext().startActivity(myIntent);

                    }else{
                        Button button = findViewById(R.id.button);
                        button.setText("Appuer pour + d'images");
                        new AlertDialog.Builder(ItemActivity.this)
                                .setTitle("Aucune autre message pour ce produit")
                                .setMessage("Aucune autre message pour ce produit")
                                .show();
                    }
				}
			});






    }

    public class MyAsyntask extends AsyncTask<String, String, String> {

        ArrayList<String> listeDate = new ArrayList<String>();
        String success="true";
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            URL url; // url
            HttpURLConnection urlConnection = null;
            try{
                //Prepare URL
                // DATE
                url = WebServiceUrlMusee.builSearchDemonstrations();
                // request connection
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();
                JSONResponseHandlerDate jsonResponseHandlerDate = new JSONResponseHandlerDate(listeDate);
                jsonResponseHandlerDate.readJsonStream(in);
            }
            catch (Exception e){
                e.printStackTrace();
            }
            finally {
                if(urlConnection != null){
                    urlConnection.disconnect();
                }
            }

            return success;
        }

        @Override
        protected void onPostExecute(String str) {
            for(String s : listeDate){
                String[] datee = s.split(":");
                if((datee[0]).equals(idItem)){
                    TextView date = findViewById(R.id.demos);
                    date.setText(datee[1]);
                }
            }
        }
    }
}
