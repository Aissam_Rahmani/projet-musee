package com.example.projetmusee;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class DbHelper extends SQLiteOpenHelper {

    private static final String TAG = DbHelper.class.getSimpleName();

    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "musee.db";

    public static final String TABLE_NAME = "musee";

    public static final String _ID = "_id";
    public static final String COLUMN_ITEM_ID = "idItem";
    public static final String COLUMN_ITEM_NAME = "name";
    public static final String COLUMN_ITEM_YEAR = "year";
    public static final String COLUMN_ITEM_CATEGORIES = "categories";
    public static final String COLUMN_ITEM_DESCRIPTION = "description";
    public static final String COLUMN_TIME_FRAME = "timeFrame";
    public static final String COLUMN_ITEM_BRAND = "brand";
    public static final String COLUMN_ITEM_THUMBNAIL="thumbnail";
    public static final String COLUMN_TECH_DETAILS = "technicDetails";
    public static final String COLUMN_ITEM_WORKING = "working";
    public static final String COLUMN_ITEM_PICTURE = "pictures";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        final String SQL_CREATE_ITEM_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY," +
                COLUMN_ITEM_ID + " TEXT NOT NULL, " +
                COLUMN_ITEM_NAME + " TEXT, " +
                COLUMN_ITEM_YEAR + " TEXT, " +
                COLUMN_ITEM_CATEGORIES + " TEXT, " +
                COLUMN_ITEM_DESCRIPTION + " TEXT, " +
                COLUMN_TIME_FRAME + " TEXT, " +
                COLUMN_ITEM_BRAND + " TEXT, " +
                COLUMN_TECH_DETAILS + " TEXT, " +
                COLUMN_ITEM_WORKING + " TEXT, " +
                COLUMN_ITEM_PICTURE + " TEXT, " +
                " UNIQUE (" + COLUMN_ITEM_ID + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_ITEM_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_NAME); // drops the old database
        onCreate(db); // run Oncreateto get new database

    }

    /**
     * Fills ContentValues result from a Team object
     */
    private ContentValues fill(Item item) {

        //        values.put(_ID, team.getId());

        ContentValues values = new ContentValues();
        values.put(COLUMN_ITEM_ID, item.getIdName());
        values.put(COLUMN_ITEM_NAME, item.getName());
        values.put(COLUMN_ITEM_YEAR, item.getYear());
        String categ = TextUtils.join("&",
                item.getCategories());
        values.put(COLUMN_ITEM_CATEGORIES, categ);
        values.put(COLUMN_ITEM_DESCRIPTION,
                item.getDescription());
        String timeframe = TextUtils.join("&",
                item.getTimeFrame());
        values.put(COLUMN_TIME_FRAME, timeframe);
        values.put(COLUMN_ITEM_BRAND, item.getBrand());
        String caracteristique = TextUtils.join("&",
                item.getTechDetails());
        values.put(COLUMN_TECH_DETAILS, caracteristique);
        values.put(COLUMN_ITEM_WORKING, item.getWorking());
        String pictures = TextUtils.join("&",
                item.getPicture());
        values.put(COLUMN_ITEM_PICTURE, pictures);

        return values;
    }

    public Item cursortoItem(Cursor cursor) {



        String[] listImage = (cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_PICTURE))).split("&");
        ArrayList<String> arrayLisImage = new ArrayList<String>();
        for(String elt : listImage)
        {
            arrayLisImage.add(elt);
        }


        String[] caraceristiques = (cursor.getString(cursor.getColumnIndex(COLUMN_TECH_DETAILS))).split("&");
        ArrayList<String> listCaracteristique = new ArrayList<String>();
        for(String eltCar : caraceristiques)
        {
            listCaracteristique.add(eltCar);
        }

        String[] casCat = (cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_CATEGORIES))).split("&");
        ArrayList<String> categorie = new ArrayList<String>();
        for(String element : casCat)
        {
            categorie.add(element);
        }

        String[] caddate = (cursor.getString(cursor.getColumnIndex(COLUMN_TIME_FRAME))).split("&");
        ArrayList<String> listdate = new ArrayList<String>();
        for(String date : caddate)
        {
            listdate.add(date);
        }



        Item item = new Item(cursor.getLong(cursor.getColumnIndex(_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_NAME)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_YEAR)),
                categorie,
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_DESCRIPTION)),
                listdate,
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_BRAND)),
                listCaracteristique,
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_WORKING)),
                arrayLisImage
        );

        return item;
    }


//  true if the Item was added to the table ; false otherwise (case when the pair (team, championship) is
//      already in the data base)


    public boolean addItem(Item item) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = fill(item);

        Log.d(TAG, "adding: "+ item.getName()+" with id="+ item.getIdName());

        // Inserting Row
        // The unique used for creating table ensures to have only one s
        // If rowID = -1, an error occured
        long rowID = db.insertWithOnConflict(TABLE_NAME, null, values, CONFLICT_IGNORE);
        db.close();

        return (rowID != -1);
    }


    /**
     * Returns a list on all the teams of the data base
     */

    public ArrayList<Object> getAllItems(String string) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null,
                null, null, null, null, string, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        ArrayList<Object> res = new ArrayList<Object>();
        while(!cursor.isAfterLast()){

            res.add(cursortoItem(cursor));
            cursor.moveToNext();
        }

        return res;
    }


    /**
     * Returns a cursor on all the item of the data base
     */
    public Cursor fetchAllItems() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null,
                null, null, null, null, COLUMN_ITEM_NAME +" ASC", null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }








    // Item by category
    public ArrayList<Object> tableItemsByCategory(ArrayList<String> categories) {

        ArrayList<Object> res = new ArrayList<Object>();
        SQLiteDatabase db = this.getReadableDatabase();
        for(String value : categories){

            res.add(value);

            String[] argument = {"%"+value+"%"};
            Cursor cursor = db.query(TABLE_NAME, null,
                    COLUMN_ITEM_CATEGORIES +" like ?", argument, null, null, COLUMN_ITEM_NAME +" ASC", null);
            if (cursor != null) {
                cursor.moveToFirst();
            }
            while(!cursor.isAfterLast()){
                res.add(cursortoItem(cursor));
                cursor.moveToNext();
            }
        }

        return res;
    }




}
