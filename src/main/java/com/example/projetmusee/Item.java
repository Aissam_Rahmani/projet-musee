package com.example.projetmusee;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Item implements Parcelable {

    public static final String TAG = Item.class.getSimpleName();

    private long id; // used for the _id colum of the db helper

    private String idName;
    private String name;
    private String year;


    private  String thumbail;
    private ArrayList<String> categories = new ArrayList<String>();
    private String description;
    private ArrayList<String> timeFrame = new ArrayList<String>();
    private String brand;
    private ArrayList<String> techDetails = new ArrayList<String>();
    private String working;
    private ArrayList<String> picture = new ArrayList<String>();

    public Item(String idName) {
        this.idName = idName;
        this.picture.add(null);
    }

    public Item(long id, String idName, String name, String year, ArrayList<String> categories, String description, ArrayList<String> timeFrame, String brand, ArrayList<String> techDetails, String working, ArrayList<String> picture) {
        this.id = id;
        this.idName = idName;
        this.name = name;
        this.year = year;
        this.categories = categories;
        this.description = description;
        this.timeFrame = timeFrame;
        this.brand = brand;
        this.techDetails = techDetails;
        this.working = working;
        this.picture = picture;
    }

    public long getId() {
        return id;
    }

    public String getIdName() {
        return idName;
    }

    public String getName() {
        return name;
    }

    public String getYear() {
        return year;
    }

    public ArrayList<String> getCategories() {
        return categories;
    }

    public String getDescription() {
        return description;
    }

    public ArrayList<String> getTimeFrame() {
        return timeFrame;
    }

    public String getBrand() {
        return brand;
    }

    public ArrayList<String> getTechDetails() {
        return techDetails;
    }

    public String getWorking() {
        return working;
    }

    public ArrayList<String> getPicture() {
        return picture;
    }

    public String getThumbail() {
        return thumbail;
    }

    public void setThumbail(String thumbail) {

        thumbail="https://demo-lia.univ-avignon.fr/cerimuseum/items/" + idName + "/thumbnail";
        this.thumbail = thumbail;
    }


    public void setId(long id) {
        this.id = id;
    }

    public void setIdName(String id_name) {
        this.idName = id_name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setCategories(ArrayList<String> categories) {
        this.categories = categories;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTimeFrame(ArrayList<String> timeFrame) {
        this.timeFrame = timeFrame;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setTechDetails(ArrayList<String> techDetails) {
        this.techDetails = techDetails;
    }

    public void setWorking(String working) {
        this.working = working;
    }
   // Vider la liste
    public void viderListImg(){
        this.picture.clear();
    }

    public void setPicture(ArrayList<String> picture) {
        this.picture = picture;
    }


    @Override
    public String toString() {
        return this.name+" = "+this.idName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(idName);
        dest.writeString(name);
        dest.writeString(year);
        dest.writeList(categories);
        dest.writeString(description);
        dest.writeList(timeFrame);
        dest.writeString(brand);
        dest.writeList(techDetails);
        dest.writeString(working);
        dest.writeList(picture);
    }


    public static final Creator<Item> CREATOR = new Creator<Item>()
    {
        @Override
        public Item createFromParcel(Parcel source)
        {
            return new Item(source);
        }

        @Override
        public Item[] newArray(int size)
        {
            return new Item[size];
        }
    };

    public Item(Parcel in) {
        this.id = in.readLong();
        this.idName = in.readString();
        this.name = in.readString();
        this.year = in.readString();
        this.categories = in.readArrayList(null);
        this.description = in.readString();
        this.timeFrame = in.readArrayList(null);
        this.brand = in.readString();
        this.techDetails = in.readArrayList(null);
        this.working = in.readString();
        this.picture = in.readArrayList(null);
    }
}
