package com.example.projetmusee;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;



/*
 * Parse to JSON the response of the teams API
 *  by the Web service
 * pars A List of Categories if response was
 * successfully analyzed
 */


public class JSONResponseHandlerCategories {

    private static final String TAG = JSONResponseHandlerCategories.class.getSimpleName();

    private ArrayList<String> listCategories;


    public JSONResponseHandlerCategories(ArrayList<String> ListCategories) {
        this.listCategories = ListCategories;
    }

    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readCategoriesArray(reader);
        } finally {
            reader.close();
        }
    }

    public void readCategoriesArray(JsonReader reader) throws IOException {
        reader.beginArray();
        while (reader.hasNext()) {
            String cat=reader.nextString();
            listCategories.add(cat);
        }
        reader.endArray();
    }
}
