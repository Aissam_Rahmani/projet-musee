package com.example.projetmusee;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.SearchView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {



    DbHelper dbHelper;
    MyAdapter adapter;
    ArrayList<Object> itemsList = new ArrayList<Object>();
    RecyclerView myrecyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        dbHelper = new DbHelper(this);
        if(dbHelper.fetchAllItems().getCount() < 1)
        {

            AllArtefactMuse fillM = new AllArtefactMuse();
            fillM.execute();

        }


        itemsList = dbHelper.getAllItems("name asc");
        myrecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        myrecyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MyAdapter(itemsList);
        myrecyclerView.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // On crée un menuinflater

        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        // Crée une reference vers serach view
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        // to replace the actionsearch key bord
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.alphabetique) {
            itemsList.clear();// vider la liste
            // Trier selon le nom en ordre alphabétique
            itemsList.addAll(dbHelper.getAllItems("name asc"));
            adapter.notifyDataSetChanged();
            return true;
        }
        if (id == R.id.chronologique) {
            itemsList.clear();
            itemsList.addAll(dbHelper.getAllItems("year asc"));
            adapter.notifyDataSetChanged();
            return true;
        }
        if (id == R.id.categorie) {

            AsyntaskCategorie allCatAsync = new AsyntaskCategorie();
            allCatAsync.execute();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    public class AllArtefactMuse extends AsyncTask<String, String, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
        String success="true";
        @Override
        protected String doInBackground(String... params) {

            ArrayList<String> listOfid = new ArrayList<String>();
            Item item;
            URL url;

            //
            HttpURLConnection urlConnection = null;
            try{
                //
                // Prepar URL
                url = WebServiceUrlMusee.buildSearchAllId();
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();

                JSONResponseHandlerIds jsonResponseHandlerIds = new JSONResponseHandlerIds(listOfid);
                // read data
                jsonResponseHandlerIds.readJsonStream(in);
            }
            catch (Exception e){
                e.printStackTrace();
            }
            finally {
                if(urlConnection != null){
                    urlConnection.disconnect();
                }
            }

            for(String id : listOfid){
                try{

                    // Prepar url

                    url = WebServiceUrlMusee.buildSearchItemInformation(id);
                    urlConnection = (HttpURLConnection) url.openConnection();
                    InputStream in = urlConnection.getInputStream();

                    item = new Item(id);

                    JSONResponseHandlerItem jsonResponseHandlerItem = new JSONResponseHandlerItem(item);
                    jsonResponseHandlerItem.readJsonStream(in);
                    dbHelper.addItem(item);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                finally {
                    if(urlConnection != null){
                        urlConnection.disconnect();
                    }
                }
            }

            return success;
        }

        @Override
        protected void onPostExecute(String s) {
            itemsList.clear();
            itemsList.addAll(dbHelper.getAllItems("name asc"));
            adapter.notifyDataSetChanged();
        }
    }

    public class AsyntaskCategorie extends AsyncTask<String, String, String>{

        ArrayList<String> categorie = new ArrayList<String>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            URL url;
            HttpURLConnection urlConnection = null;
            try{
                // Categorie
                //  Prepare url
                url = WebServiceUrlMusee.buildSearchAllCategories();
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream input = urlConnection.getInputStream();
                JSONResponseHandlerCategories jsonResponseHandlerCategories = new JSONResponseHandlerCategories(categorie);
                // Read data
                jsonResponseHandlerCategories.readJsonStream(input);
            }
            catch (Exception e){
                e.printStackTrace();
            }
            finally {
                if(urlConnection != null){
                    urlConnection.disconnect();
                }
            }

            return "true";
        }

        @Override
        protected void onPostExecute(String s) {
            itemsList.clear();
            itemsList.addAll(dbHelper.tableItemsByCategory(categorie));
            adapter.notifyDataSetChanged();
        }
    }

    public static void monALterDialog(Context context){

        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle("Error");
        alertDialog.setMessage("You are not connected");
        alertDialog.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                alertDialog.dismiss();
            }
        }, 10000);
    }





   /* class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable{

         public static ArrayList<Object> listdesItems;

    /// on stocke les item. car on supprime a chaque fois. si non on
    // perdera lecontenu
    private ArrayList<Object> listDesItemsFull;
    public static Object obj;



    }*/










}

