package com.example.projetmusee;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {




    public static ArrayList<Object> listdesItems;

    // Nouvelle dont laquelle on stocke les item. car on supprime a chaque fois. si non on
    // perdera lecontenu
    private ArrayList<Object> listDesItemsFull;
    public static Object itemm;


    public MyAdapter(ArrayList<Object> listdesItems){
        this.listdesItems = listdesItems;
        //Créé la liste pour l'utilisé independament de listDesItems
        listDesItemsFull = new ArrayList<>(listdesItems);
    }

    @Override
    public int getItemViewType(int position) {
        if (listdesItems.get(position) instanceof Item) {
            return o;
        } else {
            return s;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if(viewType == o){
            View view = inflater.inflate(R.layout.row, parent, false);
            return new RowHolderItem(view);
        }
        else{
            View view = inflater.inflate(R.layout.row, parent, false);
            return new SectionViewHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position){
        itemm = listdesItems.get(position);
        // chaque item de sa position
        if(itemm instanceof Item){
            ((RowHolderItem) holder).display((Item) itemm);

        }
        else{
            ((SectionViewHolder) holder).display((String) itemm);
        }

    }



    @Override
    public int getItemCount(){
            return listdesItems.size();
    }

    @Override
    public Filter getFilter() {
        return filtre;
    }
    private Filter filtre = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ArrayList<Object> filteredList = new ArrayList<Object>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(listDesItemsFull);
            }
            else {
                // La strin achercher
                String monFiltPat = constraint.toString().toLowerCase().trim();
                for (Object itemm : listdesItems) {
                            Item item=(Item) itemm;
                        if (item.getName().toLowerCase().contains(monFiltPat)
                                || TextUtils.join(" ; ",item.getCategories()).toLowerCase().contains(monFiltPat) ) {
                            filteredList.add(item);
                        }
                        else if((item.getYear()) != null && item.getYear().contains(monFiltPat)){
                            filteredList.add(item);
                        }
                        else if((item.getTimeFrame()) != null && TextUtils.join(" ; ",item.getTimeFrame()).toLowerCase().contains(monFiltPat)){
                            filteredList.add(item);
                        }
                        else if((item.getBrand()) != null && item.getBrand().toLowerCase().contains(monFiltPat)){
                            filteredList.add(item);
                        }
                        else if((item.getTechDetails()) != null && TextUtils.join(" ; ",item.getTechDetails()).toLowerCase().contains(monFiltPat)){
                            filteredList.add(item);
                        }

                }
            }
            FilterResults resultSearche = new FilterResults();
            resultSearche.values = filteredList;
            return resultSearche;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //Vider la liste

            listdesItems.clear();
            listdesItems.addAll((ArrayList) results.values);
            notifyDataSetChanged();
        }
    };



     class SectionViewHolder extends RecyclerView.ViewHolder{

         TextView sectionCategorie=null;

        public SectionViewHolder(View view){
            super(view);
            sectionCategorie = view.findViewById(R.id.sectionCategorie);
        }

        public void display(String titre){
            sectionCategorie.setText(titre);
        }
    }



    // My asynctask IMAGE:

    public static class LoadImageTask extends AsyncTask<String, Void, Bitmap> {

        ImageView imageIcone;
        Item team;

        public LoadImageTask(ImageView imageView, Item team) {
            this.imageIcone = imageView;
            this.team = team;
        }

        protected Bitmap doInBackground(String... urls){
            Bitmap image = null;
            if(team.getThumbail() != null){
                try{
                    InputStream in = new java.net.URL(team.getThumbail()).openStream();
                    image = BitmapFactory.decodeStream(in);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
            return image;
        }


        @Override
        protected void onPostExecute(Bitmap result) {

            imageIcone.setImageBitmap(result);

        }


    }


    class RowHolderItem extends RecyclerView.ViewHolder{


         TextView year;
         TextView name;
         TextView categories;
         ImageView thumbnail;

        public RowHolderItem(final View view){

            super(view);
            year=view.findViewById(R.id.yeaar);
            name = view.findViewById(R.id.name);
            categories = view.findViewById(R.id.categories);
            thumbnail = view.findViewById(R.id.thumbnail);


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(view.getContext(), ItemActivity.class);
                    intent.putExtra("item", (Item) listdesItems.get(getAdapterPosition()));
                    v.getContext().startActivity(intent);
                }
            });
        }




        public void display(Item item){
            name.setText(item.getName());
            year.setText(item.getYear());

            categories.setText(TextUtils.join(" ; ", item.getCategories()));
            try{
                String urlT = WebServiceUrlMusee.buildSearchPngThumbnail(item.getIdName()).toString();
                Picasso.get().load(urlT).into(thumbnail);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }
     int o = 1;
     int s = 2;



}
