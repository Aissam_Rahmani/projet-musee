package com.example.projetmusee;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ActivityAvecImage extends AppCompatActivity {

    MyAdapterOfImageItem adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recyclview);
        Intent myIntent = getIntent();

        final Item item = myIntent.getParcelableExtra("possedImages");

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerViewLisImg);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MyAdapterOfImageItem(item.getPicture(), item.getIdName());
        recyclerView.setAdapter(adapter);
    }









    // Crrer un adaptateur a mon recyclerVIew des image JPEG

    class MyAdapterOfImageItem extends RecyclerView.Adapter<MyAdapterOfImageItem.MyViewHolder>{

        String itemID;
        String imgJPEG;
        ArrayList<String> imageID;

        public MyAdapterOfImageItem(ArrayList<String> imageID, String itemID){
            this.imageID = imageID;
            this.itemID = itemID;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.row_image_item, parent, false);
            return new MyViewHolder(view);
        }


        @Override
        public void onBindViewHolder(MyViewHolder holder, int position){
            imgJPEG = imageID.get(position);
            holder.display(imgJPEG);

        }

        @Override
        public int getItemCount(){
                return imageID.size();

        }

        public  class MyViewHolder extends RecyclerView.ViewHolder{

             TextView infosImgJpeg;
             ImageView imageItem;

            public MyViewHolder(final View view){
                super(view);

                imageItem = view.findViewById(R.id.imageJpegItem);
                infosImgJpeg = view.findViewById(R.id.infosImgJpeg);

            }

            public void display(String pic){
                String[] tabPictures = pic.split(":");

                infosImgJpeg.setText(tabPictures[1]);
                try{


                    String url = WebServiceUrlMusee.buildSearchJpegImage(itemID, tabPictures[0]).toString();

                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }







}
