package com.example.projetmusee;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


 /**
 * reponse done by service web
 * https://demo-lia.univ-avignon.fr/cerimuseum/ids
 * Responses must be provided in JSON.
 *
 */


public class JSONResponseHandlerIds {

    private static final String TAG = JSONResponseHandlerIds.class.getSimpleName();

    private ArrayList<String> lesIds = new ArrayList<String>();


    public JSONResponseHandlerIds(ArrayList<String> lesIds) {
        this.lesIds = lesIds;
    }


     /**
      * Parse to JSON the response of the teams API
      * @param response done by the Web service
      * @return A List of id  if response was
      * successfully analyzed
      */


    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readIds(reader);
        } finally {
            reader.close();
        }
    }

    public void readIds(JsonReader reader) throws IOException {
        reader.beginArray();
        while (reader.hasNext()) {
            lesIds.add(reader.nextString());
        }
        reader.endArray();
    }
}
