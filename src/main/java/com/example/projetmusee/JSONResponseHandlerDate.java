package com.example.projetmusee;



import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class JSONResponseHandlerDate {

    private static final String TAG = JSONResponseHandlerDate.class.getSimpleName();

    private ArrayList<String> date;


    public JSONResponseHandlerDate(ArrayList<String> date) {
        this.date = date;
    }

    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readDate(reader);
        } finally {
            reader.close();
        }
    }

    private void readItemDate(JsonReader reader) throws IOException {
        while (reader.hasNext()) {
            String obj = reader.nextName();
            String nextStr = reader.nextString();
            date.add(obj.concat(":".concat(nextStr)));

        }
    }


    public void readDate(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            readItemDate(reader);
        }
        reader.endObject();
    }

}
